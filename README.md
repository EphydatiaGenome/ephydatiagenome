# *Ephydatia muelleri* genome project #

## Genome assembly ##
[Download full assembly here](https://bitbucket.org/EphydatiaGenome/ephydatiagenome/downloads/renumbered_final_nb.fa.gz), 1444 scaffolds, 322Mb

All other files, including Supplementary Files for our genome paper, are available from:
[All other files here](https://bitbucket.org/EphydatiaGenome/ephydatiagenome/downloads/)

Please also see our project webpage [Ephybase here](https://spaces.facsci.ualberta.ca/ephybase/)